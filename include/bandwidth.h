#ifndef BANDWIDTH
#define BANDWIDTH

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <math.h>

#define DEF_LOWER_BOUND 20000

bool finished = false;
int num_vertecies;
int max_value, count = 0, current, cost;
char* filename;

typedef struct edge {
	int vertex1;
	int vertex2;
} edge;

typedef struct {
	int minimum_bandwidth;
	int lower_bound;
	int upper_bound;
} bandwidth_info;

typedef struct graph{

	edge* edge_list;
	int max_vertex_value;
	int num_vertecies;
	int num_of_edges;
	bandwidth_info info;
} graph;

graph* current_graph;

int* current_best_perm;

void process_solution(int[], int);

void construct_candidates(int[], int, int[], int*);

bool is_a_solution(int);

void backtrack(int[], int);

void generate_permutations(void);

void print_info(graph*, char*);

void exit_program(FILE*, int);

bool should_proceed(int[], int);


































#endif

