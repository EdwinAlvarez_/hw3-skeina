#include "bandwidth.h"

int main(int argc, char** argv){
	if(argc == 1){
		fprintf(stderr, "Please enter a file directory as first argument\n");
		exit(1);
	}
	else
		filename = argv[1];

	current_graph = malloc(sizeof(graph));
	FILE *fileStream = fopen(filename, "r");
	if(fileStream == NULL){
		fprintf(stderr, "Path %s doesn't exist\n", filename);
		free(current_graph);
		exit(1);
	}

	fscanf(fileStream, "%d", &current);
	if(feof(fileStream)){
		fprintf(stderr, "No number of vertecies found\n");
		exit_program(fileStream, 1);
	}

	current_graph->num_vertecies = current;

	fscanf(fileStream, "%d", &current);
	if(feof(fileStream)){
		fprintf(stderr, "No number of edges found\n");
		exit_program(fileStream, 1);
	}
	current_graph->num_of_edges = current;

	edge edge_array[current_graph->num_of_edges + 1];
	int best_perm[current_graph->num_vertecies + 1];
	current_best_perm = best_perm;
	current_graph->num_of_edges = current;
	current_graph->edge_list = edge_array;
	current_graph->info.upper_bound = 0;
	current_graph->info.lower_bound = DEF_LOWER_BOUND;
	current_graph->info.minimum_bandwidth = DEF_LOWER_BOUND;
	cost = current_graph->num_vertecies;
	int ver1, ver2, j = 1;
	edge current_edge;

	do{
		fscanf(fileStream, "%d", &ver1);
		if(feof(fileStream))
			break;
			

		fscanf(fileStream, "%d", &ver2);
		if(feof(fileStream))
			break;

		current_edge = (struct edge) {ver1, ver2};
		current_graph->edge_list[j] = current_edge;
		
		j++;
	}while(!feof(fileStream));

	generate_permutations();
	print_info(current_graph, filename);
	//<----------- to be deleted ------------>
	printf("Number of permutations read: %d\n", count);
	exit_program(fileStream, EXIT_SUCCESS);
}

void backtrack(int current_solution[], int k){
	int candidates[num_vertecies];
	int ncandidates;
	int i;

	if(is_a_solution(k))
		process_solution(current_solution, k);
	else{
		k = k + 1;
		construct_candidates(current_solution, k, candidates, &ncandidates);
		for (i = 0; i < ncandidates; ++i){
			current_solution[k] = candidates[i];
			if(should_proceed(current_solution, k))
				backtrack(current_solution, k);
			else
				continue;

			if(finished) return;
		}
	}
}

void process_solution(int current_solution[], int k){
	int i, vertex1, vertex2, current_distance, current_max_bandwidth = 0;
	count++;
	int B[current_graph->num_vertecies];
	for(i = 1; i <= k; i++)
		B[current_solution[i]] = i;

	for (int i = 1; i <= current_graph->num_of_edges; ++i){
		vertex1 = current_graph->edge_list[i].vertex1;
		vertex2 = current_graph->edge_list[i].vertex2;
		current_distance = abs(B[vertex2] - B[vertex1]);

		if(current_distance > current_max_bandwidth)
			current_max_bandwidth = current_distance;

		if(current_distance > current_graph->info.upper_bound)
			current_graph->info.upper_bound = current_distance;

		if(current_distance < current_graph->info.lower_bound)
			current_graph->info.lower_bound = current_distance;
	}

	if(current_max_bandwidth < current_graph->info.minimum_bandwidth){
		current_graph->info.minimum_bandwidth = current_max_bandwidth;
		cost = current_max_bandwidth;
		for(i = 1; i <= k; i++)
			current_best_perm[i] = current_solution[i];
	}
}

bool is_a_solution(int k){
	return k == num_vertecies;
}

void construct_candidates(int current_solution[], int k, int c[], int* ncandidates){
	int i;
	bool in_perm_bit_vector[max_value + 1]; /* who is in the permutation? */
	for (i=1; i<max_value + 1; i++) in_perm_bit_vector[i] = false;
	for (i=1; i<k; i++) in_perm_bit_vector[ current_solution[i] ] = true;
			
	*ncandidates = 0;
	for (i=1; i <= max_value; i++)
		if (in_perm_bit_vector[i] == false) {
			c[ *ncandidates] = i;
			*ncandidates = *ncandidates + 1;
		}
}

void generate_permutations(){
	num_vertecies = current_graph->num_vertecies;
	max_value = current_graph->num_vertecies;

	int a[num_vertecies];
	for (int i = 0; i < num_vertecies; ++i)
		a[i] = 0;
	
	backtrack(a, 0);
}

void print_info(graph* graph, char* filename){
	printf("****************************************************************\n");
	printf("File: %s\n", filename);
	printf("Lower Bound: %d ,  Upper Bound: %d ,  Minimum Bandwidth: %d\n", 
		graph->info.lower_bound,
		graph->info.upper_bound,
		graph->info.minimum_bandwidth
	);
	printf("Permutation: ");

	for (int i = 1; i <= graph->num_vertecies; ++i)
		printf("%d, ", current_best_perm[i]);
	printf("\n");
	printf("****************************************************************\n");
}

bool should_proceed(int partial_solution[], int len){
	int i, vertex1, vertex2, current_cost, max_cost = 0;
	int B[current_graph->num_vertecies];

	for (int i = 1; i <= current_graph->num_vertecies; ++i)
		B[i] = -1;
	
	for(i = 1; i <= len; i++)
		B[partial_solution[i]] = i;
	
	for (int i = 1; i <= current_graph->num_vertecies; ++i)
		if(B[i] == -1)
			B[i] = len + 1;

	for (int i = 1; i <= current_graph->num_of_edges; ++i){
		vertex1 = current_graph->edge_list[i].vertex1;
		vertex2 = current_graph->edge_list[i].vertex2;
		current_cost = abs(B[vertex2] - B[vertex1]);

		if(current_cost > max_cost)
			max_cost = current_cost;
	}

	if(max_cost >= cost)
		return false;
	else
		return true;

}

void exit_program(FILE* file, int exit_num){
	free(current_graph);
	fclose(file);
	exit(exit_num);
}
