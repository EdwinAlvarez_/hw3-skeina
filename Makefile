CFLAGS = -g -c -Wall -Werror -Iinclude -pedantic -Wextra -lm

all: clean bandwidth

bandwidth: bandwidth-converter 
	gcc -o bin/bandwidth build/bandwidth.o -lm

create-dir:
	@mkdir -p bin
	@mkdir -p build

bandwidth-converter: create-dir src/bandwidth.c
	gcc $(CFLAGS) -c src/bandwidth.c -o build/bandwidth.o

bandwidth-debug: create-dir src/bandwidth.c
	gcc -g -DDEBUG $(CFLAGS) -c src/bandwidth.c -o build/bandwidth.o

debug: clean bandwidth-debug
	gcc -o bin/utf build/bandwidth.o -lm

clean:
	rm -rf bin
	rm -rf build
